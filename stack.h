#ifndef _STACK_LINKED_LIST_H_
#define _STACK_LINKED_LIST_H_

#include "user.h"


typedef struct
{
    STK_ENTRY_DATATYPE entry;
    struct Stack_node* next;
}tStack_node;

typedef struct
{
    tStack_node* top;
    int counter;
}tStack;

void STK_Create(tStack* ps);
void STK_Push(tStack* ps, STK_ENTRY_DATATYPE e);
void STK_Pop(tStack* ps, STK_ENTRY_DATATYPE* pe);
int STK_Full(tStack* ps);
int STK_Empty(tStack* ps);
void STK_Top(tStack* ps, STK_ENTRY_DATATYPE* pe);
int  STK_Size(tStack* ps);
void STK_Clear(tStack* ps);
#endif // _STACK_LINKED_LIST_H_

