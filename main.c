#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

int main()
{
    tStack stk;
    int a;
    int b;

    STK_Create(&stk);

    if(!STK_Full(&stk))
    {
        STK_Push(&stk, 5);

    }

    if(!STK_Empty(&stk))
    {
        STK_Pop(&stk, &a);
    }
    printf("%d\n", a);

    if(!STK_Full(&stk))
     {
        STK_Push(&stk, 7);
        if(!STK_Full(&stk))
        {
            STK_Push(&stk, 9);
        }
     }
     STK_Top(&stk, &b);
     printf("%d\n", b);

    b = STK_Size(&stk);
    printf("%d\n", b);

    STK_Clear(&stk);
    STK_Top(&stk, &b);
    printf("%d\n", b);
    return 0;
}
