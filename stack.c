#include "stack.h"
#include <stdio.h>
#include <stdlib.h>

void STK_Create(tStack* ps)
{
    ps->top = NULL;
    ps->counter = 0;
}

void STK_Push(tStack* ps, STK_ENTRY_DATATYPE e)
{
    tStack_node *pe = (tStack_node*) malloc(sizeof(tStack_node));
    pe->next = ps->top;
    pe->entry = e;
    ps->top = pe;
    ps->counter++;
}

void STK_Pop(tStack* ps, STK_ENTRY_DATATYPE* e)
{
    tStack_node* pe = ps->top->next;
    *e = ps->top->entry;
    free((tStack*)ps->top);
    ps->top = pe;
    ps->counter--;
}
int STK_Full(tStack* ps)
{
    return  0;
}

int STK_Empty(tStack* ps)
{
    if(ps->counter)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

void STK_Top(tStack* ps, STK_ENTRY_DATATYPE* pe)
{
    if(!ps->top)
    {
        *pe = 0;
    }
    else
    {
    *pe = ps->top->entry;
    }
}

int  STK_Size(tStack* ps)
{
    return (ps->counter);
}

void STK_Clear(tStack* ps)
{
//    tStack_node* pq = ps->top;
//    tStack_node* pe = ps->top;
//
//    while(pe != NULL)
//    {
//        pq = pe->next;
//        free(pe);
//        pe = pq;
//    }
//    ps->counter = 0;

    tStack_node* pq;
    while(ps->top)
    {
        pq = ps->top->next;
        free(ps->top);
        ps->top = pq;
    }
}
